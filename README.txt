
ABOUT ZFZBLANK
-----------

It's the 040 base theme for the Drupal 8 sites we develop. The goal is simple: act as an intermediary to remove all unnecessary markup and CSS.